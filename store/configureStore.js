import {createStore, combineReducers} from 'redux';
import rootReducer from '../reducer/rootReducer'
import thunk from 'redux-thunk'
import {applyMiddleware, compose} from 'redux'
import {reduxFirestore, getFirestore, firestoreReducer} from 'redux-firestore'
import {reactReduxFirebase, getFirebase} from 'react-redux-firebase'
import fbConfig from '../config/fbConfig'



// added enhancers to store
const configureStore = () => {
    return createStore(rootReducer, 
        compose(
            applyMiddleware(thunk.withExtraArgument({getFirebase, getFirestore})),
            reduxFirestore(fbConfig),
            reactReduxFirebase(fbConfig, {useFirestoreForProfile: true, userProfile: 'users', attachAuthIsReady: true})
        )
    )
}

export default configureStore 
