notifications.map((note) => {
        return(
          <View style={styles.note}>
            <Text style={styles.noteText}>{note.user}</Text>
            <Text style={styles.noteText}>{note.content}</Text>
            <Text style={styles.noteText}>{moment(note.time.toDate()).fromNow()}</Text>
          </View>
        )
      })