//This is where store is added to the app as well as where the app is being rendered

import {AppRegistry} from 'react-native';
import React, { Component } from 'react';
import App from './App';
import {name as appName} from './app.json';
import {Provider} from 'react-redux';


import configureStore from './store/configureStore'


const store = configureStore()

const RNRedux = () => (
    <Provider store={store}>
        <App />
    </Provider>
)


 store.firebaseAuthIsReady.then(() => {})

AppRegistry.registerComponent(appName, ()=>RNRedux)