import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TextInput, ScrollView, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux'
import {signOut} from '../../store/actions/authActions'

class Profile extends Component {
  render() {
    const{profile} = this.props
    //console.warn(profile.initials) 
    return (
      <View style={styles.container}>
          <Text>{profile.initials} is signed in</Text>
          <TouchableOpacity onPress={this.props.signOut}>
            <Text>LOG OUT</Text>
          </TouchableOpacity>
      </View>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return{
    signOut: () => dispatch(signOut())
  }
}

const mapStateTopProps= (state) => {
  return {
    profile: state.firebase.profile
  }
}

export default connect(mapStateTopProps, mapDispatchToProps) (Profile)


const styles = StyleSheet.create({
  container: {
      flex: 1,
      paddingLeft:100,
      alignContent:'center',
      justifyContent: 'center'
  },
  
});