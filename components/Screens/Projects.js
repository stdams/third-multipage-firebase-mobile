import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TextInput, ScrollView, TouchableOpacity} from 'react-native';
import ProjectSummary from '../Projects/ProjectSummary'
import {connect} from 'react-redux'
import {firestoreConnect} from 'react-redux-firebase'
import {compose} from 'redux'

class Projects extends Component {
  render() {
    //console.warn(this.props)
    const {projects} = this.props
    //console.warn(projects)
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.headerText}>PROJECTS</Text>
        </View>
        <ScrollView style={styles.projectsummary}>
          <ProjectSummary projects={projects}/>
        </ScrollView>
      </View>
        
        
    );
  }
}

const mapStateToProps = (state) => {
  //console.warn(state.firestore.ordered)
  return {
    //projects: state.project.projects,
    projects: state.firestore.ordered.projects
} 
}

export default compose(
  connect(mapStateToProps),
  firestoreConnect([
    {collection:'projects', orderBy:['createdAt','desc'] }
  ])
) (Projects)

const styles = StyleSheet.create({
  projectsummary: {
      flex:1,
      backgroundColor: '#455a64'
  },
  container: {
      flex: 1,
  },
  header: {
      paddingLeft: 20,
      backgroundColor: '#455a64',
      borderBottomWidth: 10,
      borderBottomColor: '#ededed',
  },
  headerText: {
    color: 'white',
    fontSize: 20,
    //fontStyle:
    padding: 20

  }
  
});