import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TextInput, ScrollView, TouchableOpacity} from 'react-native';
import Notification from '../Notification/Notification'
import {connect} from 'react-redux'
import {firestoreConnect} from 'react-redux-firebase';
import {compose} from 'redux'

class Notifications extends Component {
  render() {
    const {notifications} =this.props
    //console.warn(notifications)
    return (
      <Notification notifications={notifications}/>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    notifications: state.firestore.ordered.notifications
  }
}

export default compose(
  connect (mapStateToProps),
  firestoreConnect([
    {collection: 'notifications', orderBy:['time', 'desc']}
  ]) 
)(Notifications)