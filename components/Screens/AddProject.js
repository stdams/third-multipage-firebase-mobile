import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TextInput, ScrollView, TouchableOpacity, StatusBar} from 'react-native';
import FormAddProject from '../Projects/FormAddProject'


class AddProject extends Component {
  state ={
    project: {}
  }
  render() {
    return (
      <View style={styles.container}>
        <View style>
            <FormAddProject project={this.state.project} />
            <View style={styles.SignInTextContainer}>
              <Text style={styles.signInText}>Feel free to add a Project... </Text>
            </View>
        </View>
      </View>
    );
  }
}



export default AddProject;







const styles = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: '#455a64',
      alignItems: 'center',
      justifyContent: 'center'
  },
  SignInTextContainer: {
    flexGrow: 1,
    alignItems: 'flex-end',
    justifyContent: 'center',
    paddingVertical: 16,
    flexDirection: 'row'
  },
  signInText: {
    color:'white',
    fontSize: 16
  },
  
});