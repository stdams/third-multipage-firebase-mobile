import React, {Component} from 'react';
import {SafeAreaView, StatusBar, Platform, StyleSheet, Text, View, TextInput, ScrollView, TouchableOpacity, Image, Dimensions} from 'react-native';
import Category from '../Homes/Category'


const {height, width}= Dimensions.get('window')

class Home extends Component {

  componentDidMount() {
    this.startHeaderHeight = 80
    if (Platform.OS== 'android'){
      this.startHeaderHeight = 100+ StatusBar.currentHeight
    } 
  }

  render() {
    return (
    <SafeAreaView style={{flex:1, backgroundColor:'#455a64' }}>
      <View styles={styles.container}>
        <View style={{ height: 80, backgroundColor: '#455a64',borderBottomWidth: 1,borderBottomColor: '#dddddd' }}>
          <View style={styles.View3}>
            <Image source={require('../auth/Picture1.png')} size={20} style={styles.profilePicture}/>
            <TextInput style={styles.TextInput1} underlineColorAndroid="transparent" placeholderTextColor="white" placeholder="Check Out Our Projects"/>
          </View>
        </View>
        <ScrollView scrollEventThrottle={16}>
          <View style={styles.View5}>
            <Text style={styles.Text1}>What Project Do you want to View</Text>
            <View style={styles.View6}>
              <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                <Category/>
                <Category/>
                <Category/>
                <Category/>
                <Category/>
              </ScrollView>
            </View>
            <View style={{marginTop: 40, paddingHorizontal:20,}}>
              <Text style={{fontSize:24, fontWeight:'700'}}>Introducing Projects</Text>
              <Text style={{fontWeight:'100', marginTop:10}}>type in a new project to begin</Text>
              <View style={{width:width-40, height:200, marginTop:20, }}>
                <Image source={require('../auth/Picture1.png')} size={20} style={{flex:1, height:null, width:null, resizeMode:'cover', borderRadius: 5, borderWidth: 1, borderColor:'#dddddd'}}/>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    </SafeAreaView>

      
    );
  }
}

export default Home

const styles = StyleSheet.create({
  container: {
    flex: 1,
    
  },
  View3: {
    flexDirection: 'row',
    padding:10,
    backgroundColor: 'rgba(255, 255, 255, 0.3)',
    marginHorizontal: 20,
    shadowOffset: {width:0,height:0},
    shadowColor:'black',
    shadowOpacity: 0.2,
    elevation:1,
    marginTop: Platform.OS == 'android'? 30 : null,
    borderRadius: 30
  },
  profilePicture: {
    marginRight: 10,
    height: 24,
    width: 24,
  },
  TextInput1: {
    flex:1, 
    fontWeight: '700',
    //backgroundColor: 'rgba(255, 255, 255, 0.3)'
  },
  Text1: {
    fontSize: 24,
    fontWeight:'700',
    paddingHorizontal: 20,
  },
  View5: {
    flex:1,
    backgroundColor: '#455a64',
    paddingTop: 20
  },
  View6: {
    height: 130,
    marginTop:20
  }


});