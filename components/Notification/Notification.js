import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TextInput, ScrollView, TouchableOpacity} from 'react-native';
import moment from 'moment'


class Notification extends Component {
  render() {
    const {notifications} = this.props
    //console.warn(notifications)
    const notes = notifications ? (
      notifications.map(note => {
        return(
          <View style={styles.note} key={note.id}>
            <Text style={styles.noteText}>{note.user}</Text>
            <Text style={styles.noteText}>{note.content}</Text>
            <Text style={styles.noteText}>{moment(note.time.toDate()).fromNow()}</Text>
          </View>
        )
      })
    ):(
        <View style={{flex:1}}>
          <Text>Loading...</Text>
        </View>
      )
    return (
      <View style = {styles.container}>
        <View style = {styles.header}>
          <Text style={styles.headerText}>NOTIFICATIONS</Text>
        </View>
        <ScrollView style={styles.projectsummary}>
            {notes}
        </ScrollView>
      </View>    
    );
  }
}

export default Notification

const styles = StyleSheet.create({
  projectsummary: {
      //flex:1,
      //backgroundColor: '#ededed'
  },
  container: {
      flex: 1,
  },
  header: {
      paddingLeft: 20,
      backgroundColor: '#455a64',
      borderBottomWidth: 10,
      borderBottomColor: '#ededed',
  },
  headerText: {
    color: 'white',
    fontSize: 20,
    //fontStyle:
    padding: 20

  },
  note: {
    position: 'relative',
    padding: 20,
    paddingRight: 100,
    paddingTop:20,
    borderBottomWidth: 10,
    borderLeftWidth: 10,
    borderRightWidth: 10,
    borderLeftColor: '#ededed',
    borderRightColor: '#ededed',
    borderBottomColor: '#ededed',
    borderRadius: 20
  },
  noteText: {
    paddingLeft: 20,
    borderLeftWidth: 10,
    borderLeftColor: '#E91E63',
  }
  
});