import React,{ Component } from "react";
import {StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native';

class Category extends Component {
    render () {
        return (
            <View style={{height: 130, width:130,marginLeft:20, borderWidth:0.5, borderColor:'#dddddd', borderRadius:10}}>
                <View style={{flex:2}}>
                    <Image source={require('../auth/Picture1.png')} size={20} style={{flex:1, width: null, height: null, resizeMode: 'cover'}}/>
                </View>
                <View style={{flex: 2, paddingLeft: 10, paddingTop:10}}>
                    <Text>Home</Text>
                </View>
            </View>
        )
    }
}

export default Category