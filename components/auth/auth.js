import React, {Component} from 'react';
import {View} from 'react-native';
import {connect} from 'react-redux'

import SignIn from './signIn'
import SignUp from './signUp'


class Auth extends Component {
    render() {

        const {count} = this.props
        var page = count? <SignUp/> : <SignIn/>
        return (
            <View style={{flex:1}}>
                {page}
            </View>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        count: state.auth.count
    }
}

export default connect (mapStateToProps) (Auth)