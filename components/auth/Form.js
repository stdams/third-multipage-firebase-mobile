import React, {Component} from 'react';
import { StyleSheet,Text, TextInput, View, TouchableOpacity,} from "react-native";


class Form extends Component {
    state = {
        email: '',
        password:''
    }

    handleSubmit= () => {
        if (this.state.email.length && this.state.password.length) {
            //console.warn(this.state)
            this.props.signIn(this.state)

        }
         
        
        
        
    }

    handleChange= (email) => 
        {this.setState({
            ...this.state,
            email: email,

        })
       
        
    }
    handleChangeP = (password) => 
        {this.setState({
            ...this.state,
            password: password,}
        )
        
    }


    render() {
        const {authError} = this.props
        const infom = authError ? (authError): null 
        return (
            <View style={styles.container}>
                <TextInput 
                style={styles.inputBox} 
                underlineColorAndroid='rgba(0,0,0,0)' 
                placeholder='Email' 
                placeholderTextColor='#ffffff'
                keyboardType='email-address'
                onChangeText={this.handleChange}
                onSubmitEditing={()=>this.password.focus()}
                />
                <TextInput 
                style={styles.inputBox} 
                underlineColorAndroid='rgba(0,0,0,0)' 
                placeholder='Password' 
                placeholderTextColor='#ffffff'
                secureTextEntry={true}
                onChangeText={this.handleChangeP}
                selectionColor='#ffffff'
                ref={(input)=> this.password = input}
                />

                <TouchableOpacity style={styles.button} onPress={this.handleSubmit}>
                    <Text style={styles.buttonText}>Signin</Text>
                </TouchableOpacity>
                <View><Text>{infom}</Text></View>
            </View>
        )
    }
}

export default Form

const styles = StyleSheet.create({
    container: {
        flexGrow:1,
        justifyContent: 'center',
        alignItems: 'center'
    },
  inputBox: {
    width: 300,
    backgroundColor: 'rgba(255, 255, 255, 0.3)',
    borderRadius: 25,
    paddingHorizontal: 16,
    height: 50,
    fontSize: 16,
    color: '#ffffff',
    marginVertical: 10
  },
  buttonText: {
      fontSize: 16,
      fontWeight: '500',
      color: '#ffffff',
      textAlign: 'center'
  },
  button: {
      backgroundColor: '#1c313a',
      borderRadius: 25,
      width: 300,
      marginVertical: 10,
      paddingVertical: 10
  }
})