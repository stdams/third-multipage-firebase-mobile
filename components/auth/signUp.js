import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TextInput, ScrollView, TouchableOpacity, StatusBar} from 'react-native';
import Logo from './Logo'
import Form from './FormSignUp';
import {connect} from 'react-redux'
import {signUp, noCount} from '../../store/actions/authActions'



class SignUp extends Component {
  render() { 


    const {signUp, authError, noCount} = this.props
    return (
      <View style={styles.container}>
        <View>
            <Logo/>
            <Form signUp={signUp} authError={authError}/>
            <View style={styles.SignInTextContainer}>
              <Text style={styles.signInText}>Already have an account </Text>
              <TouchableOpacity onPress={noCount}>
                <Text style={styles.signupbutton}>SignIn</Text>
              </TouchableOpacity>
            </View>
        </View>
      </View>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    signUp: (newUser) => dispatch(signUp(newUser)),
    noCount: () => dispatch(noCount())
  }
}

const mapStateToProps = (state) => {
  return {
    authError: state.auth.authError
  }
}

export default connect(mapStateToProps, mapDispatchToProps) (SignUp);







const styles = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: '#455a64',
      alignItems: 'center',
      justifyContent: 'center'
  },
  SignInTextContainer: {
    flexGrow: 1,
    alignItems: 'flex-end',
    justifyContent: 'center',
    paddingVertical: 16,
    flexDirection: 'row'
  },
  signInText: {
    color:'rgba(255,255,255,0.7)',
    fontSize: 16
  },
  signupbutton: {
    color: '#ffffff',
    fontSize: 16,
    fontWeight: '500'
  }
  
});
