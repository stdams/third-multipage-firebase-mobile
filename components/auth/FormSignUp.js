import React, {Component} from 'react';
import { StyleSheet,Text, TextInput, View, TouchableOpacity,} from "react-native";


class FormSignUp extends Component {
    state = {
        email:'',
        password:'',
        firstName:'',
        lastName:''
    }

    handleSubmit= () => {
        //console.warn(this.props)
        this.props.signUp(this.state)
    }

    handleChange= (email) => {
        this.setState({
            ...this.state,
            email: email,

        })
    }
    handleChangeP = (password) => {
        this.setState({
            ...this.state,
            password: password,

        })
    }
    handleChangef = (first) => {
        this.setState({
            ...this.state,
            firstName: first,

        })
    }
    handleChangel = (last) => {
        this.setState({
            ...this.state,
            lastName: last,

        })
    }
    

    render() {
        const {authError} = this.props
        const infom = authError ? (authError) : null
        return (
            <View style={styles.container}>
            <TextInput 
                style={styles.inputBox} 
                underlineColorAndroid='rgba(0,0,0,0)' 
                placeholder='Email' 
                placeholderTextColor='#ffffff'
                keyboardType='email-address'
                onChangeText={this.handleChange}
                onSubmitEditing={()=>this.password.focus()}
                />
                <TextInput 
                style={styles.inputBox} 
                underlineColorAndroid='rgba(0,0,0,0)' 
                placeholder='Password' 
                placeholderTextColor='#ffffff'
                secureTextEntry={true}
                selectionColor='#ffffff'
                id="password"
                onChangeText={this.handleChangeP}
                ref={(input)=> this.password = input}
                />
            <TextInput 
                style={styles.inputBox} 
                underlineColorAndroid='rgba(0,0,0,0)' 
                placeholder='First Name' 
                placeholderTextColor='#ffffff'
                keyboardType='name-phone-pad'
                id= "firstName"
                onChangeText={this.handleChangef}
                //onSubmitEditing={()=>this.password.focus()}
                />
            <TextInput 
                style={styles.inputBox} 
                underlineColorAndroid='rgba(0,0,0,0)' 
                placeholder='Last Name' 
                placeholderTextColor='#ffffff'
                id="lastName"
                onChangeText={this.handleChangel}
                keyboardType='name-phone-pad'
                //onSubmitEditing={()=>this.password.focus()}
                />

                <TouchableOpacity style={styles.button} onPress={this.handleSubmit}>
                    <Text style={styles.buttonText}>Signup</Text>
                </TouchableOpacity>
                <Text>{infom}</Text>
            </View>
        )
    }
}

export default FormSignUp

const styles = StyleSheet.create({
    container: {
        flexGrow:1,
        justifyContent: 'center',
        alignItems: 'center'
    },
  inputBox: {
    width: 300,
    backgroundColor: 'rgba(255, 255, 255, 0.3)',
    borderRadius: 25,
    paddingHorizontal: 16,
    height: 50,
    fontSize: 16,
    color: '#ffffff',
    marginVertical: 10
  },
  buttonText: {
      fontSize: 16,
      fontWeight: '500',
      color: '#ffffff',
      textAlign: 'center'
  },
  button: {
      backgroundColor: '#1c313a',
      borderRadius: 25,
      width: 300,
      marginVertical: 10,
      paddingVertical: 10
  }
})