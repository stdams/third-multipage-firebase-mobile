import React, {Component} from 'react';
import { StyleSheet, Text, View, Image} from "react-native";


class Logo extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Image style={styles.image} source={require('./Picture1.png')}/>
                <Text style={styles.logoText}>Welcome to the App</Text>
            </View>
        )
    }
}

export default Logo

const styles = StyleSheet.create({
    image: {
        width: 40,
        height: 70
    },
    logoText: {
        marginVertical: 15,
        fontSize: 18,
        color: 'rgba(255, 255, 255, 0.7)'
    },
    container: {
        flexGrow:1,
        justifyContent: 'flex-end',
        alignItems: 'center'
    }
})