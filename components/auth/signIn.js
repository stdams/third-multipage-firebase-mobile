import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TextInput, ScrollView, TouchableOpacity, StatusBar} from 'react-native';
import Logo from './Logo'
import Form from './Form';
import {connect} from 'react-redux'
import {signIn, count1} from '../../store/actions/authActions'



class SignIn extends Component {

  render() {
    const {signIn, authError, count1} = this.props
    return (
      <View style={styles.container}>
        <View>
            <Logo/>
            <Form signIn={signIn} authError={authError}/>
            <View style={styles.SignInTextContainer}>
              <Text style={styles.signInText}>Don't have an account yet? </Text>
              <TouchableOpacity TouchableOpacity onPress={count1}>
                <Text style={styles.signupbutton}>SignUp</Text>
              </TouchableOpacity>
            </View>
        </View>
      </View>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    signIn: (credentials) => dispatch(signIn(credentials)),
    count1: () => dispatch(count1())
  }
}

const mapStateToProps = (state) => {
  return {
    authError: state.auth.authError
  }
}

export default connect(mapStateToProps, mapDispatchToProps) (SignIn);







const styles = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: '#455a64',
      alignItems: 'center',
      justifyContent: 'center'
  },
  SignInTextContainer: {
    flexGrow: 1,
    alignItems: 'flex-end',
    justifyContent: 'center',
    paddingVertical: 16,
    flexDirection: 'row'
  },
  signInText: {
    color:'rgba(255,255,255,0.7)',
    fontSize: 16
  },
  signupbutton: {
    color: '#ffffff',
    fontSize: 16,
    fontWeight: '500'
  }
  
});
