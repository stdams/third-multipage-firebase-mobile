import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TextInput, ScrollView, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux'


class ProjectDetails extends Component {
  render() {
        return (
          <View style={styles.note} key={proj.id}>
            <Text style={styles.noteText1}>{proj.title}</Text>
            <Text style={styles.noteText}>Posted by the Net Ninja</Text>
            <Text style={styles.noteText}>3rd September, 2am</Text>
          </View>
        )
    }
}

export default connect (mapStateToProps) (ProjectDetails)


const styles = StyleSheet.create({
  note: {
    position: 'relative',
    padding: 20,
    paddingRight: 100,
    paddingTop:20,
    borderBottomWidth: 10,
    borderLeftWidth: 10,
    borderRightWidth: 10,
    borderLeftColor: '#455a64',
    borderRightColor: '#455a64',
    borderBottomColor: '#455a64',
    borderRadius: 20,
    backgroundColor:'white'
  },
  noteText1: {
    paddingLeft: 20,
    borderLeftWidth: 10,
    borderLeftColor: '#E91E63',
    fontSize: 25
  },
  noteText: {
    paddingLeft: 20,
    borderLeftWidth: 10,
    borderLeftColor: '#E91E63',
  }
  
});