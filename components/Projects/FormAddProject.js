import React, {Component} from 'react';
import { StyleSheet,Text, TextInput, View, TouchableOpacity,} from "react-native";
import {createProject} from '../../store/actions/projectActions'
import {connect} from 'react-redux'


class FormAddProject extends Component {
    state = {
        title: '',
        content: ''
    }

    handleSubmit= () => {
        if (this.state.title.length && this.state.content.length) {
            //console.warn(this.state)
            this.props.newProject(this.state)
            this.setState({
                title:'',
                content:''
            })
        }
        
    }

    handleChange= (title) => {
        this.setState({
            ...this.state,
            title:title

        })
    }
    handleChangeP = (content) => {
        this.setState({
            ...this.state,
            content: content

        })
    }
    
    

    render() {
        return (
            <View style={styles.container}>
            <TextInput 
                style={styles.inputBox} 
                underlineColorAndroid='rgba(0,0,0,0)' 
                placeholder='Title' 
                placeholderTextColor='sienna'
                value={this.state.title}
                keyboardType='name-phone-pad'
                onChangeText={this.handleChange}
                />
                <TextInput 
                style={styles.inputBox} 
                underlineColorAndroid='rgba(0,0,0,0)' 
                placeholder='Content'
                value={this.state.content} 
                placeholderTextColor='sienna'
                keyboardType='name-phone-pad'
                selectTextOnFocus={false}
                onChangeText={this.handleChangeP}
                />
            

                <TouchableOpacity style={styles.button} onPress={this.handleSubmit}>
                    <Text style={styles.buttonText}>Create</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        newProject: (project) => dispatch(createProject(project))
    }
}

export default connect(null, mapDispatchToProps) (FormAddProject)

const styles = StyleSheet.create({
    container: {
        flexGrow:1,
        justifyContent: 'center',
        alignItems: 'center'
    },
  inputBox: {
    width: 300,
    backgroundColor: 'snow',
    borderRadius: 25,
    paddingHorizontal: 16,
    height: 50,
    fontSize: 16,
    color: 'black',
    marginVertical: 10
  },
  buttonText: {
      fontSize: 16,
      fontWeight: '500',
      color: '#ffffff',
      textAlign: 'center'
  },
  button: {
      backgroundColor: '#1c313a',
      borderRadius: 25,
      width: 300,
      marginVertical: 10,
      paddingVertical: 10
  }
})