import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TextInput, ScrollView, TouchableOpacity} from 'react-native';
import moment from 'moment'

class ProjectSummary extends Component {
  render() {
    const {projects} = this.props
    //console.warn(projects)
    const projs = projects ? (
      projects.map(proj => {
        return (
          <View style={styles.note} key={proj.id}>
            <Text style={styles.noteText1}>{proj.title}</Text>
            <Text style={styles.noteText}>Posted by {proj.authorFirstName} {proj.authorLastName}</Text>
            <Text style={styles.noteText}>{moment(proj.createdAt.toDate()).calendar()}</Text>
          </View>)
      })
    ):(
      <View style={styles.note}>
        <Text style={styles.noteText2}>Loading Posts...</Text>
      </View>
    )
    return (
          <View>
            {projs}
          </View>
    ) 
    
  }
}

export default ProjectSummary


const styles = StyleSheet.create({
  note: {
    position: 'relative',
    padding: 20,
    paddingRight: 100,
    paddingTop:20,
    borderBottomWidth: 10,
    borderLeftWidth: 10,
    borderRightWidth: 10,
    borderLeftColor: '#455a64',
    borderRightColor: '#455a64',
    borderBottomColor: '#455a64',
    borderRadius: 20,
    backgroundColor:'white'
  },
  noteText1: {
    paddingLeft: 20,
    borderLeftWidth: 10,
    borderLeftColor: '#E91E63',
    fontSize: 25
  },
  noteText: {
    paddingLeft: 20,
    borderLeftWidth: 10,
    borderLeftColor: '#E91E63',
  },
  noteText2: {
    //paddingLeft: 100,
    flex:1,
    justifyContent:'center',
    alignContent:'center',
    borderLeftWidth: 10,
    borderLeftColor: '#E91E63',
    fontSize: 25
  }
});