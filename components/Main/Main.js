import React, {Component} from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native';

import {decode, encode} from 'base-64'

if (!global.btoa) {
    global.btoa = encode;
}

if (!global.atob) {
    global.atob = decode;
}

import Auth from '../auth/auth'
import Dashboard from './Dashboard'
import {connect} from 'react-redux'




class Main extends Component {
  
  render() {
    
    
    const {auth, profile} = this.props;
    //console.warn(profile.initials)
    const link = auth.uid ? <Dashboard profile={profile.initials}/>:<Auth/>
    return (
      <View style={{flex:1}}>
        {link}
      </View>
      
    )
  }    
}

const mapStateToProps = (state) => {
  //console.warn(state.firebase.auth)
  return {
    auth: state.firebase.auth,
    profile: state.firebase.profile
  }
}



export default connect (mapStateToProps) (Main);











