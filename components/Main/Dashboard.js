import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TextInput, ScrollView, TouchableOpacity, Image} from 'react-native';
import {createBottomTabNavigator} from 'react-navigation'
//import Icon from 'react-native-vector-icons/Ionicons'

import Home from '../Screens/Home';
import AddProject from '../Screens/AddProject';
import Projects from '../Screens/Projects';
import Notifications from '../Screens/Notifications';
import Profile from '../Screens/Profile';



export default createBottomTabNavigator({

  
  Home:{
    screen: Home,
    navigationOptions:{
      tabBarLabel: 'Home',
      tabBarIcon: ({tintColor})=>(
        <Image source={require('../auth/Picture1.png')} style={styles.profilePicture}/>
      )
    }
  },
  AddProject: {
    screen: AddProject,
    navigationOptions:{
      tabBarLabel: 'AddProject',
      tabBarIcon: ({tintColor})=>(
        <Image source={require('../auth/Picture1.png')} style={styles.profilePicture}/>
      )
    }
  },
  Notifications: {
    screen: Notifications,
    navigationOptions:{
      tabBarLabel: 'Notifications',
      tabBarIcon: ({tintColor})=>(
        <Image source={require('../auth/Picture1.png')} style={styles.profilePicture}/>
      )
    }
  },
  Projects: {
    screen: Projects,
    navigationOptions:{
      tabBarLabel: 'Projects',
      tabBarIcon: ({tintColor})=>(
        <Image source={require('../auth/Picture1.png')} style={styles.profilePicture}/>
      )
    }
  },
  Profile: {
    screen: Profile,
    navigationOptions:{
      tabBarLabel: 'Profile',
      tabBarIcon: ({tintColor})=>(
        <Image source={require('../auth/Picture1.png')} style={styles.profilePicture}/>
      )
    }
  }
},{
  tabBarOptions:{
    activeTintColor: 'red',
    inactiveTintColor: 'grey',
    style:{
      backgroundColour: 'white',
      borderTopWidth:0,
      shadowOffset:{width:5, height:3},
      shadowColor:'black',
      shadowOpacity: 0.5,
      elevation: 5
    }
  }
})




const styles = StyleSheet.create({
  profilePicture: {
      height: 24,
      width: 24,
      //tintColor: tintColor
  },
  

}
);