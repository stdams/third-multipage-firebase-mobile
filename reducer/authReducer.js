import{Alert} from 'react-native'

const initState = {
    authError: null,
    count: ''
}

const authReducer = (state = initState, action) => {
    switch(action.type) {
        case 'COUNT_ONE' :
            Alert.alert('Please Sign up')
            return {
                ...state,
                count:'1234'
            }
        case 'NO_COUNT' :
            Alert.alert('please Sign in')
            return {
                ...state,
                count:''
            }
        case'NEW_USER_CREATED':
            Alert.alert('Account Creation Successful')
            return {
                ...state,
                authError: null
            }
        case 'NEW_USER_ERROR':
            Alert.alert('error creating user')
            return {
                ...state,
                authError: action.err.message
            }
        case 'LOGIN_ERROR':
            Alert.alert('login failed')
            return {
                ...state,
                authError: 'Login failed'
        }
        case 'LOGIN_SUCCESS':
            Alert.alert('login success')
            return {
                ...state,
                authError: null
            }
        case 'LOGOUT_SUCCESS':
            Alert.alert('You have logged Out')
        default:
            return state    
    }
}

export default authReducer