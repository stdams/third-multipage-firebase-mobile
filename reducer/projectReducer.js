import{Alert} from 'react-native'

const initState = {
    projects: [
        {id: '1', title: 'help me find peach', content: 'blah blah blah'},
        {id: '2', title: 'collect all the stars', content: 'blah blah blah'},
        {id: '3', title: 'go egg hunting', content: 'blah blah blah'},
    ]
}

const projectReducer = (state = initState, action) => {
    switch (action.type) {
        case 'CREATE_PROJECT' :
            Alert.alert('SUCCESS','Project Added Successfully')
            return state;
        case 'CREATE_PROJECT_ERROR' :
            Alert.alert('error in creating project')
            console.warn(action.err)
            return state
        default:
            return state
             
    }
}

export default projectReducer