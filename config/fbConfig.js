import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'

  // Your web app's Firebase configuration
  var firebaseConfig = {
    apiKey: "AIzaSyDGc92g3KamSqGplndS69f7_EKbVNGORbg",
    authDomain: "harmcorp.firebaseapp.com",
    databaseURL: "https://harmcorp.firebaseio.com",
    projectId: "harmcorp",
    storageBucket: "harmcorp.appspot.com",
    messagingSenderId: "314070062676",
    appId: "1:314070062676:web:b1f75395936b56b392bfc1",
    measurementId: "G-82FE2H7E6R"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  //firebase.analytics();
  //firebase.firestore().settings({timestampsInSnapshots: true})

  export default firebase
